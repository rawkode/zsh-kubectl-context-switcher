function kubectl_context_switcher() {
    while IFS='=' read -r name value context; do
        if [[ $name == 'KUBECTL_CONTEXT_SWITCHER_'* ]]; then
            if [[ $(pwd) =~ .*${value}.* ]]; then
                if [[ $(kubectl config current-context) != $context ]]; then
                    kubectl config use-context ${context}
                fi
            fi
        fi
    done < <(env)
}

chpwd_functions+=(kubectl_context_switcher)
